﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace identityApiClientMvc.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

		[Authorize]
		public IActionResult Secure()
		{
			return View();
		}

		public async Task<IActionResult> Logout()
		{
			await HttpContext.Authentication.SignOutAsync("Cookies");
			await HttpContext.Authentication.SignOutAsync("oidc");

			return new RedirectToActionResult("Index", "Home", new object());
		}
    }
}
