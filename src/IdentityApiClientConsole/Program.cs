﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

namespace IdentityApiClientConsole
{
    class Program
    {
		static void Main(string[] args)
		{
			//CallIdentityApi(GetClientCredential).Wait();
			CallIdentityApi(GetResourceOwner).Wait();
		}

		async static Task CallIdentityApi(Func<DiscoveryResponse, Task<TokenResponse>> tokenResponseRetriever)
		{
			// discover endpoints from metadata
			var disco = await DiscoveryClient.GetAsync("http://localhost:5000");

			// request token
			var tokenResponse = await tokenResponseRetriever(disco);

			if (tokenResponse.IsError)
			{
				Console.WriteLine(tokenResponse.Error);
				return;
			}

			Console.WriteLine(tokenResponse.Json);

			// call api
			var client = new HttpClient();
			client.SetBearerToken(tokenResponse.AccessToken);

			var response = await client.GetAsync("http://localhost:5001/identity");
			if (!response.IsSuccessStatusCode)
			{
				Console.WriteLine(response.StatusCode);
			}
			else
			{
				var content = await response.Content.ReadAsStringAsync();
				Console.WriteLine(JArray.Parse(content));
			}
		}

		async static Task<TokenResponse> GetClientCredential(DiscoveryResponse disco)
		{
			// request token
			var tokenClient = new TokenClient(disco.TokenEndpoint, "client", "secret");
			return await tokenClient.RequestClientCredentialsAsync("api1");
		}
	
		async static Task<TokenResponse> GetResourceOwner(DiscoveryResponse disco)
		{
			// request token
			var tokenClient = new TokenClient(disco.TokenEndpoint, "ro.client", "secret");
			return await tokenClient.RequestResourceOwnerPasswordAsync("alice", "password", "api1");
		}
	}
}
