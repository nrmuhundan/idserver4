﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace IdentityApiClientMvcHybrid.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

		[Authorize]
		public IActionResult Secure()
		{
			return View();
		}

		[Authorize]
		public async Task<IActionResult> Api()
		{
			var accessToken = await HttpContext.Authentication.GetTokenAsync("access_token");

			var client = new HttpClient();
			//client.SetBearerToken(accessToken);
			client.DefaultRequestHeaders.Authorization 
			    = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
			var content = await client.GetStringAsync("http://localhost:5001/identity");

			ViewBag.Json = JArray.Parse(content).ToString();
			return View();
		}

		public async Task<IActionResult> Logout()
		{
			await HttpContext.Authentication.SignOutAsync("Cookies");
			await HttpContext.Authentication.SignOutAsync("oidc");

			return new RedirectToActionResult("Index", "Home", new object());
		}
	}
}
